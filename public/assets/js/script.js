console.log("Script linked suxesfule")

function callTime()
{
    let temp = new Date();
    let date = temp.getFullYear() + "/" + (temp.getMonth() + 1) + "/" + temp.getDay();
    let time = " " + temp.getHours() + ":" + temp.getMinutes() + ":" + temp.getSeconds();
    let time_Day = date+ " " +time;

    //Output works
    //console.log(time_Day);
    //This doesn't ?! WHY
    let stamp = document.getElementById('timeStamp');
    //console.log(stamp + " tell me WHY it's null bro");
    stamp.innerHTML = time_Day; 
    //console.log(time_Day + " tell me TIME\n");
    //console.log(stamp);
}

//Calls a function intensively for set argument time
let interval = setInterval(callTime, 1000);

//Bring the Canvas in
const canvas = document.getElementById("gameCanvas");
const context = canvas.getContext("2d");

//Sprite variables
let frameSizeX = 25;
let frameSizeY = 36;
let spriteSizeX = 75;
let spriteSizeY = 108;
let speed = 5;
let frameX = 0;
let frameY = 0;
let newFrameX = 0;
let newFrameY = 0;
let iteration = 0;
let increment = 0.25;

//Enum "class" for direction
const directionEnum = Object.freeze(
{
    north: 0,
    south: 1,
    west: 2,
    east: 3,
    none: 4
});
let m_facing = directionEnum.none;

//Sprite position
let posX = (canvas.width / 2 ) - spriteSizeX;
let posY = (canvas.height / 2) - spriteSizeY; 

let playerSprite = new Image();
//Draw function
function draw()
{
    playerSprite.src = "assets/images/spriteSheet.png";
    playerSprite.onload = function()
    {
        context.drawImage(playerSprite, frameX, frameY, frameSizeX, frameSizeY, posX, posY, spriteSizeX, spriteSizeY);
        console.log("Sprite drawn");
    }
    console.log("Draw function called");
}

//Event listeners
document.addEventListener("keydown", input);
document.addEventListener("keyup", input);

//input function
function input(event)
{
    if (event.type === "keydown") //When pressed
    {
        switch (event.keyCode)
        {
            
            case 37: case 65: //Debug
                console.log("West");
                console.log("Direction - " + directionEnum.west);
                m_facing = directionEnum.west;
                animate();
                posX -= speed;
                break;
            case 38: case 87: //Debug
                console.log("North");
                console.log("Direction - " + directionEnum.north);
                m_facing = directionEnum.north;
                animate();
                posY -= speed;
                break;
            case 39: case 68: //Debug
                console.log("East");
                console.log("Direction - " + directionEnum.east);
                m_facing = directionEnum.east;
                animate();
                posX += speed;
                break;
            case 40: case 83: //Debug
                console.log("South");
                console.log("Direction - " + directionEnum.south);
                m_facing = directionEnum.south;
                animate();
                posY += speed;
                break;
            default:
                break;      
        } 
    }
}
//Animation function
function animate()
{
    switch(m_facing)
    {
        case 0:
            frameX = frameSizeX * 2;
            newFrameX = frameX;
            frameY = frameSizeY;
            iteration += increment;
            if (iteration >= 0 && iteration <= 2)
            {
                newFrameY += frameY;
            }
            if(iteration >= 2)
            {
                iteration = 0;
            }
            if(newFrameY >= 73)
            {
                newFrameY = 0;
            }
            console.log("Frame Y = " +newFrameY);
            console.log("Frame X = " +newFrameX);
            console.log("Iteration = " +iteration);
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.drawImage(playerSprite, newFrameX, newFrameY, frameSizeX, frameSizeY, posX, posY, spriteSizeX, spriteSizeY);
            break;   
        case 1:
            frameX = frameSizeX * 0;
            newFrameX = frameX;
            frameY = frameSizeY;
            iteration += increment;
            if (iteration >= 0 && iteration <= 2)
            {
                newFrameY += frameY;
            }
            if(iteration >= 2)
            {
                iteration = 0;
            }
            if(newFrameY >= 73)
            {
                newFrameY = 0;
            }
            console.log("Frame Y = " +newFrameY);
            console.log("Frame X = " +newFrameX);
            console.log("Iteration = " +iteration);
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.drawImage(playerSprite, newFrameX, newFrameY, frameSizeX, frameSizeY, posX, posY, spriteSizeX, spriteSizeY);
            break;   

    }

}
//Needs to be called once
draw();
function update()
{
    callTime();
    window.requestAnimationFrame(update);
}


window.requestAnimationFrame(update);
